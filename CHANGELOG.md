# SIA Homepage changelog

The latest version of this file can be found at the master branch of the SIA Homepage repository.

## 0.4.0

### Removed (1 change)

- Clean-up of the project assets

### Fixed (3 changes, 1 of them are from the community)

- Correction of the english translation done by Fiona Derathé
- Correction of the manifest.json (description and logo usage)
- Correction of the error handling test for the App component

### Changed (1 change)

- Use the Link component from Material UI for <a/> tag for markdown rendering

### Added (2 changes)

- Add ontology and meta tag for Twitter, Facebook and Google
- Add a linter CI to enforce style for new contributor
- New section to show internal training and made an available PDF/video support for each of them

### Other (0 change)

## 0.3.0

### Removed (2 changes)

- SIABot user is no longer displayed as a SIA member (see #9)
- Home page opening animation is removed (see #9)

### Fixed (1 change, 0 of them are from the community)

- UserRole properly return if an unknown Role is provided to it (see #4)

### Changed (4 changes)

- Improve usage of npm in CI/CD
- Updating dependencies
- Improve responsibility by passing from md to lg breakpoint and changing the drawer type
- Improve coverage of the application (removing axios request and assets from coverage)

### Added (5 changes)

- The website detect user preference regarding dark mode to automatically enable it (see #9)
- The website has now a dark mode option (see #9)
- An About view has been added (see #6)
- The website has now an english version. A button has been added to the toolbar in order to switch easily between the french and english version (see #10)
- WhiteSource Renovate bot is now working on this project with a specific configuration

### Other (0 change)

## 0.2.0

### Removed (3 changes)

- Remove unused dependencies
- Remove usage of Django backend
- Remove unused assets

### Fixed (0 change, 0 of them are from the community)

### Changed (2 changes)

- Change the information about project in [package.json](./package.json) file
- Revamp the project structure to have better code separation

### Added (6 changes)

- Adding CI/CD and environment configuration (Staging and Deployment)
- Adding issue and MR templates
- Adding Changelog, Contributing and env file
- Allow usage of GitLab personal token from `.env` file (see #2)
- Adding Axios methods and views to use GitLab REST API in order to see members and projects (see #2)
- Adding test to our app

### Other (0 change)

## 0.1.0

### Removed (0 change)

### Fixed (0 change, 0 of them are from the community)

### Changed (1 change)

- Protect master against direct changes

### Added (3 changes)

- Adding base CRA webapp
- Adding Home page
- Adding basic project page

### Other (0 change)
