[![Licence GPL](http://img.shields.io/badge/license-GPL-green.svg)](http://www.gnu.org/licenses/quick-guide-gplv3.fr.html)
[![coverage report](https://gitlab.com/sia-insa-lyon/dev/homepagesia/homepagesia-front/badges/master/coverage.svg)](https://gitlab.com/sia-insa-lyon/homepagesia/homepagesia-front/commits/master)
[![pipeline status](https://gitlab.com/sia-insa-lyon/dev/homepagesia/homepagesia-front/badges/master/pipeline.svg)](https://gitlab.com/sia-insa-lyon/homepagesia/homepagesia-front/commits/master)

# SIA Homepage

Website for SIA's projets and members

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Built With
- [Create-React-App](https://facebook.github.io/create-react-app) &mdash; Our bootstrap utility.
- [ReactJS](https://reactjs.org/) &mdash; Our main front framework.
- [MaterialUI](https://material-ui.com/) &mdash; Our style framework.

And a lot of other dependencies you can find in package.json file.


## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
SIA Homepage - Website for SIA's projet
Copyright (C) 2019 SIA INSA Lyon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```