import React from 'react';
import {act} from "@testing-library/react";
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {Typography} from "@material-ui/core";

import {getAllIssuesByGroup} from "../Request/GitLab/issue";
import {getAllProjectByRange} from "../Request/GitLab/project";
import {issueListHomeView} from "./Request/issueList.js";
import {projectListHomeView} from "./Request/projectList.js";

import Home from "../View/Home";
import AccordionIssue from "../Component/AccordionIssue";
import AccordionItem from "../Component/AccordionItem";
import {local} from "../Assets/Lang/fr_fr";

Enzyme.configure({adapter: new Adapter()});

jest.mock("../Request/GitLab/issue");
jest.mock("../Request/GitLab/project");
const fnError = jest.fn();

describe('Examining the rendering of home view', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('renders without crashing', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueListHomeView));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);
        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then(() => {
                    component.update();
                });
            });
        });
        expect(getAllIssuesByGroup).toBeCalled();
        expect(getAllProjectByRange).toBeCalled();

        expect(component.render());
        component.unmount();
    });

    it('renders issues when API works', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueListHomeView));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);
        expect(component.exists(AccordionItem)).toBe(true);
        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);

        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then(() => {
                    component.update();
                });
            });
        });

        expect(component.find(AccordionItem).first().props()["loading"]).toBeFalsy();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length);
        component.unmount();
    });

    it('renders skeleton when issue API fail', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.reject("APIFailTestIssue"));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);

        expect(component.exists(AccordionItem)).toBe(true);
        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);

        await act(async () => {
            getAllIssuesByGroup().then().catch(()=>{
                component.update();
            });
        });

        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);
        expect(fnError).toHaveBeenCalled();
        component.unmount();
    });

    it('renders skeleton when project API fail', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueListHomeView));
        getAllProjectByRange.mockImplementation(() => Promise.reject("APIFailTestIssue_GetProject"));

        const component = mount(<Home errorHandler={fnError}/>);

        expect(component.exists(AccordionItem)).toBe(true);
        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);

        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then().catch(()=>{
                    component.update();
                });
            });
        });

        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);
        expect(fnError).toHaveBeenCalled();
        component.unmount();
    });

    it('renders no issue when there is none to display', async () => {
        let issueEmptyList = { ...issueListHomeView};
        issueEmptyList.data = [];

        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueEmptyList));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);
        expect(component.exists(AccordionItem)).toBe(true);
        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);

        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then(() => {
                    component.update();
                });
            }).catch((err) => console.error(err));
        });

        expect(component.exists(AccordionIssue)).toBe(false);
        expect(component.find(Typography).last().text()).toBe(local.View.Home.missingIssueText);
        component.unmount();
    });

    it('allows search from issue list', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueListHomeView));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);
        expect(component.exists(AccordionItem)).toBe(true);
        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);

        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then(() => {
                    component.update();
                });
            }).catch((err) => console.error(err));
        });

        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length);

        // Check accent search
        act(() => {
            component.find('#issue-search').first().props()["onChange"]({target: {value: "événement"}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(1);

        act(() => {
            component.find('#issue-search').first().props()["onChange"]({target: {value: "evenement"}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(1);

        // Reset
        act(() => {
            component.find('#issue-search').first().props()["onChange"]({target: {value: ""}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length);

        // Two letters
        act(() => {
            component.find('#issue-search').first().props()["onChange"]({target: {value: "ta"}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(5);

        component.unmount();
    });

    it('allows to sort issue list', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueListHomeView));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);
        expect(component.exists(AccordionItem)).toBe(true);
        expect(component.find(AccordionItem).first().props()["loading"]).toBe(true);

        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then(() => {
                    component.update();
                });
            }).catch((err) => console.error(err));
        });

        // Check update select option (this sort at first since GitLab request mock is order by create)
        act(() => {
            component.find('#issue-select-filter').first().props()["onChange"]({target: {value: "2"}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length);
        expect(component.find(AccordionIssue).at(0).props()["issue"]).toBe(issueListHomeView.data[0]);
        expect(component.find(AccordionIssue).at(1).props()["issue"]).toBe(issueListHomeView.data[1]);
        expect(component.find(AccordionIssue).at(3).props()["issue"]).toBe(issueListHomeView.data[3]);
        expect(component.find(AccordionIssue).at(7).props()["issue"]).toBe(issueListHomeView.data[7]);

        // Check create select option, should give the same sort by default
        act(() => {
            component.find('#issue-select-filter').first().props()["onChange"]({target: {value: "1"}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length);
        expect(component.find(AccordionIssue).at(0).props()["issue"]).toBe(issueListHomeView.data[0]);
        expect(component.find(AccordionIssue).at(1).props()["issue"]).toBe(issueListHomeView.data[1]);
        expect(component.find(AccordionIssue).at(3).props()["issue"]).toBe(issueListHomeView.data[3]);
        expect(component.find(AccordionIssue).at(7).props()["issue"]).toBe(issueListHomeView.data[7]);


        // Check reset
        act(() => {
            component.find('#issue-select-filter').first().props()["onChange"]({target: {value: "2"}});
        });
        component.update();
        act(() => {
            component.find('#issue-select-filter').first().props()["onChange"]({target: {value: "0"}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length);
        expect(component.find(AccordionIssue).at(0).props()["issue"]).toBe(issueListHomeView.data[0]);
        expect(component.find(AccordionIssue).at(1).props()["issue"]).toBe(issueListHomeView.data[1]);
        expect(component.find(AccordionIssue).at(3).props()["issue"]).toBe(issueListHomeView.data[3]);
        expect(component.find(AccordionIssue).at(7).props()["issue"]).toBe(issueListHomeView.data[7]);

        component.unmount();
    });

    it('allows to filter by project', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueListHomeView));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);
        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then(() => {
                    component.update();
                });
            });
        });

        component.update();
        const handler = component.find('#multiple-filter-project-input').first().props()["onChange"];
        //Uncheck PVA to remove the only issue in mocked dataset
        act(() => {
            handler({target: {value: [projectListHomeView.data[0]]}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(1);
        expect(component.find(AccordionIssue).first().props()["project"]).toBe(projectListHomeView.data[0]);

        //Uncheck Homepage and have no issue displayed
        act(() => {
            handler({target: {value: []}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(0);
        expect(component.find(Typography).last().text()).toBe(local.View.Home.missingIssueText);

        //Check Homepage and have homepage issue
        act(() => {
            handler({target: {value: [projectListHomeView.data[14]]}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length-1);
        const filteredElement = component.find(AccordionIssue).filterWhere((n) => n.props()["project"] === projectListHomeView.data[14]);
        expect(filteredElement).toHaveLength(issueListHomeView.data.length-1);

        //Check PVA and check that filter doesn't alter the original dataset display
        act(() => {
            handler({target: {value: [projectListHomeView.data[0], projectListHomeView.data[14]]}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length);

        component.unmount();
    });

    it('allows to filter by project and seach with project filter active', async () => {
        getAllIssuesByGroup.mockImplementation(() => Promise.resolve(issueListHomeView));
        getAllProjectByRange.mockImplementation(() => Promise.resolve(projectListHomeView));

        const component = mount(<Home errorHandler={fnError}/>);
        await act(async () => {
            getAllIssuesByGroup().then(() => {
                getAllProjectByRange().then(() => {
                    component.update();
                });
            });
        });

        component.update();
        const handlerSelect = component.find('#multiple-filter-project-input').first().props()["onChange"];
        const handlerSearch = component.find('#issue-search').first().props()["onChange"];

        act(() => {
            handlerSelect({target: {value: [projectListHomeView.data[14]]}});
        });
        component.update();
        expect(component.find(AccordionIssue)).toHaveLength(issueListHomeView.data.length-1);

        act(() => {
            handlerSearch({target: {value: "dis"}});
        });
        component.update();
        const brokenFilteredElement = component.find(AccordionIssue).filterWhere((n) => n.props()["project"] !== projectListHomeView.data[14]);
        expect(brokenFilteredElement).toHaveLength(0);
        expect(component.find(AccordionIssue)).toHaveLength(3);

        component.unmount();
    });
});
