import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import HighlightCode from "../Component/HighlightCode";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of highlight component', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<HighlightCode value={"Test"}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('renders with proper language', () => {
        const content = "* Test";
        const language = "md";
        const component = mount(<HighlightCode value={content} language={language}/>);
        expect(component.find("code").first().text()).toBe(content);
        expect(component.find("code").first().props().className).toBe(language);
        component.unmount();
    });
});
