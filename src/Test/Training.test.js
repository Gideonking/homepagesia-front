import React from 'react';
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import TrainingCard from "../Component/TrainingCard";
import Training from "../View/Training";
import {TRAINING_INFO} from "../View/Content/training";

Enzyme.configure({adapter: new Adapter()});

const fnError = jest.fn();

describe('Examining the rendering of training view', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('renders without crashing', () => {
        const component = mount(<Training errorHandler={fnError}/>);
        expect(component.render());
        component.unmount();
    });

    it('renders with the proper number of card', () => {
        const component = mount(<Training errorHandler={fnError}/>);
        let sumLength = 0;
        for(let i=0; i<TRAINING_INFO.length; i++){
            sumLength += TRAINING_INFO[i].trainings.length;
        }
        expect(component.find(TrainingCard)).toHaveLength(sumLength);
        component.unmount();
    });
});
