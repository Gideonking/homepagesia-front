import React from 'react';
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import RoleComponent from "../Component/UserRoleComponent";
import RoleType from "../Component/RoleType";
import getChipElement, {getAllCategoryDetail} from "../Utils/UserRole";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the return of User Role info', () => {

    it('return a Chip component when user role is correct', () => {
        const component = mount(getChipElement(10));
        expect(component.containsMatchingElement(<RoleComponent role={RoleType.GUEST}/>)).toBe(true);
        component.unmount();
    });

    it('return a null component when user role is incorrect', () => {
        expect(getChipElement(0)).toBe(null);
        expect(getChipElement(11)).toBe(null);
        expect(getChipElement(100)).toBe(null);
        expect(getChipElement(null)).toBe(null);
        expect(getChipElement(undefined)).toBe(null);
    });

    it('return a null component when user role is incorrect', () => {
        expect(getChipElement(0)).toBe(null);
        expect(getChipElement(11)).toBe(null);
        expect(getChipElement(100)).toBe(null);
        expect(getChipElement(null)).toBe(null);
        expect(getChipElement(undefined)).toBe(null);
    });
});