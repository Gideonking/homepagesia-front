import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, {mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {Chip, Typography} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";

import AccordionItem from "../Component/AccordionItem";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of accordion component', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<AccordionItem title={"test"} id={"test"}><p>OK</p></AccordionItem>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('renders with children', () => {
        const children = <em>Test</em>;
        const component = mount(
            <AccordionItem title={"test"} id={"test"}>
                {children}
            </AccordionItem>
        );
        expect(component.containsMatchingElement(children)).toBe(true);
        component.unmount();
    });

    it('renders with title', () => {
        const component = mount(
            <AccordionItem title={"titleTest"} id={"test"}/>
        );

        expect(component.exists(Typography)).toBe(true);
        expect(component.find(Typography)).toHaveLength(1);
        expect(component.find(Typography).filterWhere((n) => n.children().text().includes("titleTest"))).toHaveLength(1);

        component.unmount();
    });

    it('renders with skeleton', () => {
        const component = mount(
            <AccordionItem title={"titleTest"} id={"test"} loading/>
        );

        expect(component.exists(Typography)).toBe(false);
        expect(component.exists(Skeleton)).toBe(true);
        expect(component.find(Skeleton)).toHaveLength(1);
        expect(component.find(Skeleton).first().props()).not.toContain("style");

        component.unmount();
    });

    it('renders with chip', () => {
        const component = mount(
            <AccordionItem title={"titleTest"} id={"test"} chip={<Chip label={"testChip"}/>}/>
        );

        expect(component.exists(Chip)).toBe(true);
        expect(component.exists(Skeleton)).toBe(false);
        expect(component.find(Chip)).toHaveLength(1);

        component.unmount();
    });

    it('renders with skeleton that have chip and text', () => {
        const component = mount(
            <AccordionItem title={"titleTest"} id={"test"} chip={<Chip label={"testChip"}/>} loading/>
        );

        expect(component.exists(Typography)).toBe(false);
        expect(component.exists(Chip)).toBe(false);
        expect(component.exists(Skeleton)).toBe(true);
        expect(component.find(Skeleton)).toHaveLength(2);
        expect(component.find(Skeleton).at(0).props("style")).not.toBe(null);
        expect(component.find(Skeleton).at(1).props("style")).not.toBe(null);

        component.unmount();
    });
});
