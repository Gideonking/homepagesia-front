import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch} from "react-router-dom";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Home from "../View/Home";
import About from "../View/About";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of the Home and About view', () => {

    it('renders without crashing for Home', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Router><Switch><Home/></Switch></Router>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('renders without crashing for About', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Router><Switch><About/></Switch></Router>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
